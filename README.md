# `potion` + `snap`

These are two lightweight shell scripts that'll help you upload to [elixi.re] on
macOS. These are only useful for developers, sorry!

[elixi.re]: https://gitlab.com/elixire/elixire

## `potion`

The file uploader. This doesn't screenshot for you -- see `snap` for that.

Pass a filename or pipe something to `stdin`. It reads your token from
`~/.elixire-token`.

This script requires `jq`. (Run `brew install jq` to install.)

```sh
# 1) upload file.png
$ potion file.png

# or 2) upload through stdin
$ cat file.png | potion
```

## `snap`

A screenshot script, which uses `screencapture(1)` (see `man 1 screencapture`).
Screenshots are saved to `~/Pictures/Screenshots`. To change this, modify the
script yourself.

If you pass `--upload`, then the script will run `potion` to upload your file
and copy the link your clipboard, while also displaying a notification.

(Make sure to edit the file to specify where `snap` can find `potion`.)

If you don't upload, the image will be copied to your clipboard instead.

## Keyboard bindings

The author uses [Alfred] to bind the scripts to keyboard shortcuts. This
requires buying the power pack, unfortunately. You can also use
[icanhazshortcut], which is free.

[alfred]: https://www.alfredapp.com
[icanhazshortcut]: https://github.com/deseven/icanhazshortcut
